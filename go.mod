module gitlab.com/hyboxu/go-module-demo-p1

go 1.20

require gitlab.com/hyboxu/go-module-demo-p3 v1.2.0

replace gitlab.com/hyboxu/go-module-demo-p3 v1.2.0 => ./internal/go-module-demo-p3
