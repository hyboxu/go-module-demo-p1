package p1

import moduledemop3 "gitlab.com/hyboxu/go-module-demo-p3"

func P3Version() string {
	return moduledemop3.Version
}
